# vim: set noexpandtab:
.PHONY: shellcheck deb man preview-man clean

shellcheck:
	shellcheck preseed-creator

deb: preseed-creator.deb

man: preseed-creator.8

preview-man:
	sed -e "s/LAST_TAG/`git tag --sort=v:refname | tail -n1`/" preseed-creator.8.md | pandoc --standalone --to man | man -l -

clean:
	rm -rf preseed-creator.8 deb/ preseed-creator.deb preseed-creator.minisig

preseed-creator.8: preseed-creator.8.md
	sed -e "s/LAST_TAG/`git tag --sort=v:refname | tail -n1`/" preseed-creator.8.md | pandoc --standalone --to man --output preseed-creator.8

preseed-creator.deb: preseed-creator preseed-creator.8
	mkdir -p deb/preseed-creator/usr/share/man/man8/ deb/preseed-creator/usr/sbin/ 
	gzip preseed-creator.8
	mv preseed-creator.8.gz deb/preseed-creator/usr/share/man/man8/
	cp preseed-creator deb/preseed-creator/usr/sbin/
	cp -r DEBIAN deb/preseed-creator/
	sed -e "s/LAST_TAG/`git tag --sort=v:refname | tail -n1`/" -i deb/preseed-creator/DEBIAN/*
	cd deb && dpkg-deb --root-owner-group --build preseed-creator
	mv deb/preseed-creator.deb preseed-creator.deb
	rm -rf deb/
