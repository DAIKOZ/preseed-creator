% PRESEED-CREATOR.SH(1) preseed-creator.sh LAST_TAG
% Luc Didry
% April 2023

# NAME
preseed-creator.sh — preseeded Debian ISO image helper

# SYNOPSIS
**preseed-creator.sh** [*OPTION*]

# DESCRIPTION
**preseed-creator.sh** will handle all the ISO manipulation for you.
Give it an ISO image to preseed and an preseed configuration file ant let it do the rest.

# OPTIONS
**-i** *image.iso*
: ISO image to preseed. If not provided, the script will download and use the latest Debian amd64 netinst ISO image

**-o** *preseeded_image.iso*
: output preseeded ISO image. Default to debian-preseed.iso

**-p** *preseed_file.cfg*
: preseed file. If not provided, the script will put "d-i debian-installer/locale string fr_FR" in the preseed.cfg file

**-w** *directory*
: directory used to work on ISO image files. Default is a temporary folder in /tmp

**-x**
: Use xorriso instead of genisoimage, to create an iso-hybrid

**-d**
: download the latest Debian amd64 netinst ISO image in the current folder and exit

**-g**
: download the latest Debian stable example preseed file into preseed_example.cfg and exit

**-v**
: activate verbose mode

**-h**
: print this help and exit

# EXIT VALUES
**0**
: Success

**1**
: Bad SHA512SUMS GPG signature for the downloaded SHA512SUMS file

**2**
: Bad checksum for the dowloaded ISO file

**3**
: Unable to create the directory to work on ISO

**4**
: The argument of option **-w** is not a directory

**5**
: Unable to go into the directory to work on ISO

**6**
: Preseed file does not exists

**7**
: Preseed file is not readable

**8**
: The provided ISO file does not exist

**9**
: The provided ISO file is not readable

**10**
: Error while mounting the ISO image

**11**
: Can’t go into the directory irmod of the directory to work on ISO

**12**
: Can’t get initrd.gz content

**13**
: Can’t disable graphic menu installer

**14**
: Error while modifying initrd.gz

**15**
: Error while modifying md5sums

**16**
: Error while creating the preseeded ISO image

# BUGS
Please, report bugs on https://framagit.org/fiat-tux/hat-softwares/preseed-creator/-/issues

# COPYRIGHT
Copyright © 2017 Luc Didry. License WTFPL: DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
